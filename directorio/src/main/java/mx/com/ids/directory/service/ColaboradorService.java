package mx.com.ids.directory.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.com.ids.directory.entity.Colaborador;
import mx.com.ids.directory.repository.ColaboradorRepository;

@Service
public class ColaboradorService {
	@Autowired  
	ColaboradorRepository colRepository;  
	
	public List<Colaborador> mostarTodo()   
	{  
	List<Colaborador> colaboradores = new ArrayList<Colaborador>();  
	colRepository.findAll().forEach(colaborador -> colaboradores.add(colaborador));  
	return colaboradores;  
	}
	
	public void guardarActualizar(Colaborador colaborador)   
	{  
	colRepository.save(colaborador);  
	} 
	
	public void eliminar(int id)   
	{  
	colRepository.deleteById(id);  
	}
	
	
	

}
