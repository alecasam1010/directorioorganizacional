package mx.com.ids.directory.controller;

import java.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import mx.com.ids.directory.entity.Colaborador;
import mx.com.ids.directory.service.ColaboradorService;

@RestController
public class ColaboradorController {
	
	@Autowired 
	ColaboradorService colServ;
	
	@GetMapping("/colaborador")  
	private List<Colaborador> getAllColaborador()   
	{  
	return colServ.mostarTodo();  
	} 
	
	@RequestMapping(value="/eliminarcolaborador", method = RequestMethod.GET)  
	private List<Colaborador> deleteColaborador(@RequestParam("id") int id)   
	{  
	colServ.eliminar(id);  
	return colServ.mostarTodo();
	
	}  
	
	@RequestMapping(value="/altaColaborador",method=RequestMethod.GET)
	public int guardarActualizar(@RequestParam("id") int id,
			@RequestParam("nombre") String nombre,
			@RequestParam("edad") String edad,
			@RequestParam("telefono") String telefono,
			@RequestParam("puesto") String puesto,
			@RequestParam("area") String area,
			@RequestParam("jefePrincipal") String jefePrincipal,
			@RequestParam("otrosJefes") String otrosJefes)
	{
		Colaborador c = new Colaborador ();
		c.setId(id);
		c.setNombre(nombre);
		c.setEdad(edad);
		c.setTelefono(telefono);
		c.setPuesto(puesto);
		c.setArea(area);
		c.setJefePirncipal(jefePrincipal);
		c.setOtrosJefes(otrosJefes);
		
		colServ.guardarActualizar(c);
		return c.getId();  
		
		
	}
	

}
