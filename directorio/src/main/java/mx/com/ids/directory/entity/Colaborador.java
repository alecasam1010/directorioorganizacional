package mx.com.ids.directory.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity  
//defining class name as Table name  
@Table  
public class Colaborador {
	
	@Id  

	@Column  
	private int id;  

	@Column  
	private String nombre; 
	@Column
	private String edad;
	@Column
	private String telefono;
	@Column
	private String puesto;
	@Column
	private String area;
	@Column
	private String jefePirncipal;
	@Column
	private String otrosJefes;
	
	public Colaborador() {}

	public Colaborador(int id, String nombre, String edad, String telefono, String puesto, String area,
			String jefePirncipal, String otrosJefes) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.edad = edad;
		this.telefono = telefono;
		this.puesto = puesto;
		this.area = area;
		this.jefePirncipal = jefePirncipal;
		this.otrosJefes = otrosJefes;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getEdad() {
		return edad;
	}

	public void setEdad(String edad) {
		this.edad = edad;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getPuesto() {
		return puesto;
	}

	public void setPuesto(String puesto) {
		this.puesto = puesto;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getJefePirncipal() {
		return jefePirncipal;
	}

	public void setJefePirncipal(String jefePirncipal) {
		this.jefePirncipal = jefePirncipal;
	}

	public String getOtrosJefes() {
		return otrosJefes;
	}

	public void setOtrosJefes(String otrosJefes) {
		this.otrosJefes = otrosJefes;
	}

	@Override
	public String toString() {
		return "Colaborador [id=" + id + ", nombre=" + nombre + ", edad=" + edad + ", telefono=" + telefono
				+ ", puesto=" + puesto + ", area=" + area + ", jefePirncipal=" + jefePirncipal + ", otrosJefes="
				+ otrosJefes + "]";
	}
	
	
	
	
	


}
