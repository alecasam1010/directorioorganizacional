package mx.com.ids.directory.repository;

import org.springframework.data.repository.CrudRepository;

import mx.com.ids.directory.entity.Colaborador;


public interface ColaboradorRepository extends CrudRepository<Colaborador, Integer> {

}
